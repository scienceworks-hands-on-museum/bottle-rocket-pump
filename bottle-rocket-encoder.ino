
#define BRIGHT 600
#define COUNT_PERIOD_MS 250
#define MIN_COUNTS_PER_PERIOD 6

const int pResistor = A0;
const int pRelay = 7;
const int ledPin=9;

int value;				  // Store value from photoresistor (0-1023)
int flashCount = 0;
bool high = 0;
unsigned long startT;

void setup(){
  pinMode(pResistor, INPUT);// Set pResistor - A0 pin as an input (optional)
  pinMode(pRelay, OUTPUT);
  Serial.begin(9600);
  startT = millis();
  digitalWrite(pRelay, 0);
}

void loop(){
  value = analogRead(pResistor);
  
  if (high && value < BRIGHT) {
    high = 0;
    flashCount++;
  } else if (!high && value >= BRIGHT) {
    high = 1;
  }

  unsigned long t = millis();
  if (t - startT >= COUNT_PERIOD_MS) {
    startT = t;

    // Actuate relay.
    if (flashCount >= MIN_COUNTS_PER_PERIOD) {
      digitalWrite(pRelay, 1);
    } else {
      digitalWrite(pRelay, 0);
    }

    flashCount = 0;
  }
}